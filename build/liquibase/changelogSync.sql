--  *********************************************************************
--  SQL to add all changesets to database history table
--  *********************************************************************
--  Change Log: src/main/resources/db/dbchangelog.xml
--  Ran at: 28/02/16 23:12
--  Against: root@localhost@jdbc:mysql://localhost:3306/poc_alquiler
--  Liquibase version: 3.1.1
--  *********************************************************************

--  Create Database Lock Table
CREATE TABLE poc_alquiler.DATABASECHANGELOGLOCK (ID INT NOT NULL, LOCKED BIT(1) NOT NULL, LOCKGRANTED datetime NULL, LOCKEDBY VARCHAR(255) NULL, CONSTRAINT PK_DATABASECHANGELOGLOCK PRIMARY KEY (ID));

--  Initialize Database Lock Table
DELETE FROM poc_alquiler.DATABASECHANGELOGLOCK;

INSERT INTO poc_alquiler.DATABASECHANGELOGLOCK (ID, LOCKED) VALUES (1, 0);

--  Lock Database
--  Create Database Change Log Table
CREATE TABLE poc_alquiler.DATABASECHANGELOG (ID VARCHAR(255) NOT NULL, AUTHOR VARCHAR(255) NOT NULL, FILENAME VARCHAR(255) NOT NULL, DATEEXECUTED datetime NOT NULL, ORDEREXECUTED INT NOT NULL, EXECTYPE VARCHAR(10) NOT NULL, MD5SUM VARCHAR(35) NULL, DESCRIPTION VARCHAR(255) NULL, COMMENTS VARCHAR(255) NULL, TAG VARCHAR(255) NULL, LIQUIBASE VARCHAR(20) NULL);

--  Create Database Lock Table
CREATE TABLE poc_alquiler.DATABASECHANGELOGLOCK (ID INT NOT NULL, LOCKED BIT(1) NOT NULL, LOCKGRANTED datetime NULL, LOCKEDBY VARCHAR(255) NULL, CONSTRAINT PK_DATABASECHANGELOGLOCK PRIMARY KEY (ID));

--  Initialize Database Lock Table
DELETE FROM poc_alquiler.DATABASECHANGELOGLOCK;

INSERT INTO poc_alquiler.DATABASECHANGELOGLOCK (ID, LOCKED) VALUES (1, 0);

INSERT INTO poc_alquiler.DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('initial changeset', 'rsalazar', '1.0', NOW(), 1, '7:fb284e7c0ac38accaeae271aeebe78d3', 'createTable (x3), addForeignKeyConstraint', 'Creadas algunas tablas de prueba', 'EXECUTED', '3.1.1');

INSERT INTO poc_alquiler.DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('production course and student data', 'jcox', '1.0', NOW(), 2, '7:1bf9e7263c4e064fb473d6da4c48b940', 'sql (x2)', 'Insert some data for our liquibase demo', 'EXECUTED', '3.1.1');

