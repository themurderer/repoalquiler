package com.prueba.alquiler.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.alquiler.repository.User;

@Repository
public interface IUserDao extends JpaRepository<User, Integer>{
	public User findOneUserById(Integer id);	
}
