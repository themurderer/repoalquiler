package com.prueba.alquiler.model;

import java.util.Date;

import com.prueba.alquiler.repository.Renting;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@ToString
public class UserBean {
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private Date create_Time;
	private Renting renting;
}
