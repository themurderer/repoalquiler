package com.prueba.alquiler.repository;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.ToString;

@Entity
@Table(name = "housing")
@Data
@ToString
public class House {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String address;

	private Integer zipCode;

	private Integer state_id;

	private String city;

	private String district;

	private byte[] image;

	@Column(columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean status;

	private Integer occupants;

	private Integer max_occupants;

	private float surface;

	private Integer tenant_id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.housing_id")
	private List<Renting> renting;

}
