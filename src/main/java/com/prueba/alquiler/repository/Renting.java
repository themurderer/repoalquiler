package com.prueba.alquiler.repository;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Table(name = "renting")
@Data
@ToString
@AssociationOverrides({ @AssociationOverride(name = "pk.occupant_id", joinColumns = @JoinColumn(name = "occupant_id")),
		@AssociationOverride(name = "pk.housing_id", joinColumns = @JoinColumn(name = "housing_id")) })
public class Renting {

	@EmbeddedId
	private RentingPK pk;

	private float price;

	private float expenses;

	private float bail;

	private Date start_date;

	private Date end_date;
}
