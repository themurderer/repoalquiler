package com.prueba.alquiler.repository;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.ToString;

@Embeddable
@Data
@ToString
public class RentingPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8891100628374226747L;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	private User occupant_id;

	@ManyToOne
	private House housing_id;
}
