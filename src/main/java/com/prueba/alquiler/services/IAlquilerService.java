package com.prueba.alquiler.services;

import com.prueba.alquiler.repository.User;

public interface IAlquilerService {

	public User getUser(Integer id);
}
