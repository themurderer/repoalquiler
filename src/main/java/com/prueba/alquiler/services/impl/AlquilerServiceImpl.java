package com.prueba.alquiler.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.alquiler.dao.IUserDao;
import com.prueba.alquiler.repository.User;
import com.prueba.alquiler.services.IAlquilerService;

@Service
@RestController
public class AlquilerServiceImpl implements IAlquilerService {

	@Autowired
	IUserDao iUser;

	@Override
	@RequestMapping("/entrada")
	public User getUser(@RequestParam(value = "id", required = true) Integer id) {
		return iUser.findOne(id);
	}

}
